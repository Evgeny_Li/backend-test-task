<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaginationRequest;
use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\JsonResponse;

class CityController extends Controller
{
	public function index(PaginationRequest $request): JsonResponse
	{
		$cities = City::query()
			->with($request->getIncludes())
			->paginate($request->getLimit());

		return CityResource::collection($cities)->response();
	}
}
