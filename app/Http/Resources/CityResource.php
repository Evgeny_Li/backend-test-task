<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
	public function toArray($request): array
	{
		return [
			'id' => $this->id,
			'title' => $this->title,
			'streets' => StreetResource::collection($this->whenLoaded('streets')),
		];
	}
}
