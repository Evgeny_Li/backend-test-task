<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

class CityFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = City::class;

	public function definition(): array
	{
		return [
			'title' => $this->faker->unique()->city,
		];
	}
}
