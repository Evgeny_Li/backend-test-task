<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Street;
use Illuminate\Database\Eloquent\Factories\Factory;

class StreetFactory extends Factory
{
	/**
	 * The name of the factory's corresponding model.
	 *
	 * @var string
	 */
	protected $model = Street::class;

	public function definition(): array
	{
		return [
			'title' => $this->faker->unique()->streetName,
			'city_id' => City::factory(),
		];
	}
}
