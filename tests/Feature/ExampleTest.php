<?php

namespace Tests\Feature;

use App\Http\Requests\PaginationRequest;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ExampleTest extends TestCase
{
	/**
	 * @dataProvider pagination_provider
	 */
	public function test_cities_streets_pagination(int $limit)
	{
		$response = $this->call(
			'GET',
			'/api/cities',
			[
				PaginationRequest::$limitKey => $limit,
				PaginationRequest::$includeKey => "streets:$limit",
			]
		);

		$assertMeta = fn(AssertableJson $json) => $json
			->has('current_page')
			->has('from')
			->has('last_page')
			->has('per_page')
			->has('to')
			->has('total');

		$response->assertStatus(200);
		$response
			->assertJson(fn(AssertableJson $json) => $json
				->has('data', $limit, fn(AssertableJson $json) => $json
					->has('id')
					->has('title')
					->has('streets', fn(AssertableJson $json) => $json
						->has('data', $limit, fn(AssertableJson $json) => $json
							->has('id')
							->has('title')
						)
						->has('meta', $assertMeta)
						->etc()
					)
				)
				->has('meta', $assertMeta)
				->etc()
			);
	}

	public function pagination_provider(): array
	{
		return [
			[10],
			[20],
			[30],
			[40],
			[50],
		];
	}

}
